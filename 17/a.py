'''
import cv2 as cv
import numpy as np
from scipy import fft
import matplotlib.pyplot as plt

img=plt.imread("fotos/img10.png")[:,:,0]
#img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
#plt.imshow(img)
imgF=fft.fft2(img)
imgF=fft.fftshift(imgF)
imgFshow=np.log(np.abs(imgF))
plt.figure()
plt.imshow(imgFshow)
plt.show


'''
import cv2 as cv
import numpy as np
from scipy import fft
import matplotlib.pyplot as plt

for j in range(0,3):
    for i in range(0,3):
        img=cv.imread("fotos/img"+str(j)+str(i)+".png")
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        imgF=fft.fft2(img)
        imgF = fft.fftshift(imgF)
        magnitude_spectrum = 1*np.log(np.abs(imgF))
        #plt.imshow(magnitude_spectrum)
        plt.imsave("resultados_de_a/img"+str(j)+str(i)+".png",magnitude_spectrum)
