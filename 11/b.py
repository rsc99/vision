import cv2
import numpy as np
def stretching(pix, r1, s1, r2, s2):
    if (0 <= pix and pix <= r1):
        return (s1 / r1)*pix
    elif (r1 < pix and pix <= r2):
        return ((s2 - s1)/(r2 - r1)) * (pix - r1) + s1
    else:
        return ((255 - s2)/(255 - r2)) * (pix - r2) + s2
def load_img(name):    
    scale=6
    img=cv2.imread(name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    width = int(img.shape[1]/scale)
    height = int(img.shape[0]/scale)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    return img

img1=load_img("a.jpg")
pixelVal_vec = np.vectorize(stretching)
r1 = 100
s1 = 0
r2 = 140
s2 = 255
img1 = pixelVal_vec(img1, r1, s1, r2, s2)


cv2.imshow("xD1",img1)
cv2.waitKey(0)
cv2.destroyAllWindows()
