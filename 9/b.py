import cv2
import numpy as np
def map2(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

scale=2
img=cv2.imread("b.jpg")
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
width = int(img.shape[1]/scale)
height = int(img.shape[0]/scale)
dim = (width, height)
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
resized[:]=resized/2
cv2.imshow("xD",resized)
cv2.waitKey(0)
cv2.destroyAllWindows()
