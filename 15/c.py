import cv2 as cv
import numpy as np

img=cv.imread("img.jpg")
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
#noise=np.random.randn((gray.shape[1],gray.shape[0]))
noise=np.random.randn(gray.shape[0],gray.shape[1])#dist. normal=media 0, desviacion std = 1
sigma=10
noise*=sigma

noisy=gray+noise
noisy.clip(0, 255)
noisy=noisy.astype('uint8')

xd=np.random.rand(gray.shape[0],gray.shape[1])
mask0=xd<0.05
mask1=0.95<xd
noisy2=np.copy(gray)
noisy2[mask0]=0
noisy2[mask1]=255
blur = cv.GaussianBlur(noisy2,(3,3),0)

xd=np.copy(noisy2)
for i in range(0,noisy2.shape[1]-3):
    for j in range(0,noisy2.shape[0]-3):
        kernel=noisy2[j:j+3,i:i+3]
        kernel=kernel.reshape(1, 9)
        kernel=np.sort(kernel)
        kernel=kernel[0][1:8]
        mean=np.sum(kernel)/7
        xd[j][i]=mean
        #xd.append(kernel)
xd2=np.copy(noisy2)
for i in range(0,noisy2.shape[1]-3):
    for j in range(0,noisy2.shape[0]-3):
        kernel=noisy2[j:j+3,i:i+3]
        kernel=kernel.reshape(1, 9)
        kernel=np.sort(kernel)
        kernel=kernel[0][2:7]
        mean=np.sum(kernel)/5
        xd2[j][i]=mean
        #xd.append(kernel)




cv.imshow("noisy",noisy2) 
cv.imshow("improve",xd) 
cv.imshow("improve_x2",xd2)    
cv.imshow("blur",blur)         
cv.waitKey(0)
cv.destroyAllWindows()
