import cv2 as cv
import numpy as np

img=cv.imread("img.jpg")
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
#noise=np.random.randn((gray.shape[1],gray.shape[0]))
noise=np.random.randn(gray.shape[0],gray.shape[1])#dist. normal=media 0, desviacion std = 1
sigma=10
noise*=sigma

noisy=gray+noise
noisy.clip(0, 255)
noisy=noisy.astype('uint8')

cv.imshow("gray",gray)
cv.imshow("noisy",noisy)

xd=np.random.rand(gray.shape[0],gray.shape[1])
mask0=xd<0.05
mask1=0.95<xd
noisy2=np.copy(gray)
noisy2[mask0]=0
noisy2[mask1]=255
cv.imshow("noisy2",noisy2)
cv.waitKey(0)
cv.destroyAllWindows()
