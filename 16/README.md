cv.bilateralFilter(img, d, sigma1, sigma2)
sigma1 en espacio de color. Cuanto mayor sea el valor, los colores más alejados van a mezclarse mas
sigma2 en espacio de coordenadas. Cuanto mayor sea su valor, más píxeles se mezclarán, dado que sus colores se encuentran dentro del rango sigmaColor.

en este caso queremos que el asfalto se suavice asi como las lineas amarillas(sin que se mezclen con un smoothing), por ello usamos valores de sigma altos, pero si d es muy bajo casi no hay efecto apreciable
