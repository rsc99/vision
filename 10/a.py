import cv2
import numpy as np

def load_img(name):    
    scale=6
    img=cv2.imread(name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    width = int(img.shape[1]/scale)
    height = int(img.shape[0]/scale)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    return img

img1=load_img("a.jpg")
img2=load_img("b.jpg")
img3=load_img("c.jpg")

cv2.imshow("xD1",img1)
cv2.imshow("xD2",img2)
cv2.imshow("xD3",img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
