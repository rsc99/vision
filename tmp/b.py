import cv2 as cv
import numpy as np
from scipy import fft
import matplotlib.pyplot as plt


img=np.zeros(480,640)

#img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
imgF=fft.fft2(img)
imgF = fft.fftshift(imgF)
magnitude_spectrum = 1*np.log(np.abs(imgF))
        #plt.imshow(magnitude_spectrum)
plt.imsave("spect2.png",magnitude_spectrum)
