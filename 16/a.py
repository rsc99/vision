import cv2 as cv
import numpy as np

img=cv.imread("img.jpg")
bilateral = cv.bilateralFilter(img, 15, 75, 75)
bilateral2 = cv.bilateralFilter(bilateral, 15, 75, 75)
cv.imshow("img",img)
cv.imshow("bilateral",bilateral)
cv.imshow("bilateral2",bilateral2)
cv.waitKey(0)
cv.destroyAllWindows()
