import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
#a
img=cv.imread("../fotos/arabe.png",0)
#b
#plt.hist(img.ravel(),256,[0,256])
#plt.show()
#c
def stretching(pix, r1, s1, r2, s2):
    if (0 <= pix and pix <= r1):
        return (s1 / r1)*pix
    elif (r1 < pix and pix <= r2):
        return ((s2 - s1)/(r2 - r1)) * (pix - r1) + s1
    else:
        return ((255 - s2)/(255 - r2)) * (pix - r2) + s2

st=np.vectorize(stretching)
r1 = 100
s1 = 1
r2 = 255
s2 = 255
img=st(img, r1, s1, r2, s2)
print(img.shape)
cv.imshow("xD1",img)
cv.waitKey(0)

