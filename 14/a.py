import cv2 as cv
import numpy as np
r = np.zeros([720, 960], dtype = int)
g = np.zeros([720, 960], dtype = int)
b = np.zeros([720, 960], dtype = int)
for i in range(0, 9):
    img=cv.imread("fotos/img"+str(i)+".jpg")
    r+=img[:,:,2]
    g+=img[:,:,1]
    b+=img[:,:,0]
    
#print(r)
r=r/10
r=r.astype('uint8')

g=g/10
g=g.astype('uint8')
b=b/10
b=b.astype('uint8')

re_img= cv.merge([b, g, r])
cv.imshow("blue",re_img)
cv.waitKey(0)
cv.destroyAllWindows()

