import cv2
import numpy as np
import matplotlib.pyplot as plt
y_line=260
def load_img(name):    
    scale=6
    img=cv2.imread(name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    width = int(img.shape[1]/scale)
    height = int(img.shape[0]/scale)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    return img

img1=load_img("a.jpg")
img2=load_img("b.jpg")
img3=load_img("c.jpg")
y1=img1[:,:]
y2=img2[:,:]
y3=img3[:,:]

x = np.linspace(0, 255)
print(len(y1))

figure, axis = plt.subplots(3)
axis[0].hist(img1.ravel(),256,[0,256]); 
axis[1].hist(img2.ravel(),256,[0,256]); 
axis[2].hist(img3.ravel(),256,[0,256]); 
plt.show()
