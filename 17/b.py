import cv2 as cv
import numpy as np
from scipy import fft
import matplotlib.pyplot as plt

img=cv.imread("lenna.png")
img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
imgF=fft.fft2(img)
imgF=fft.fftshift(imgF)
imgFshow=np.log(np.abs(imgF))
plt.imsave("lenna_spectracal.png", imgFshow)

H=cv.imread("lenna_mask.png")
H = cv.cvtColor(H, cv.COLOR_BGR2GRAY)

Ffiltro=imgF*H

Ffiltroshow=np.log(np.abs(Ffiltro))
plt.imshow(Ffiltroshow)
Ffiltro=fft.ifftshift(Ffiltro)
img_filtered=np.real(fft.ifft2(Ffiltro))
plt.imshow(img_filtered)
plt.imsave("img_filtered.png", img_filtered)
img2=cv.imread("img_filtered.png")
img2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)
cv.imwrite("img_filtered_gray.png", img2)